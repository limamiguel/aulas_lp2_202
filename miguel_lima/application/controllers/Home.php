<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

    public function index () {

        $this->load->model('ContasModel', 'conta');
        $this->conta->cria();

        $v['lista'] = $this->conta->lista();

        $html = $this->load->view('forms/forms_conta', null, true);

        $this->show($html);

    }

}