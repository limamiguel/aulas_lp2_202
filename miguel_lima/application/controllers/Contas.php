<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contas extends MY_Controller {

    public function index() {

        echo 'Lista de Todas as Contas';

    }

    public function pagar ($mes = 0, $ano = 0) {

        //cria conta
        $this->load->model('ContasModel', 'conta');
        $this->conta->cria('pagar');
        //carrega lista de contas
        $v['lista'] = $this->conta->lista('pagar');
        $v['tipo'] = 'pagar';
        //init da view com a lista de contas, tudo como string 
        $html = $this->load->view('contas/lista_contas', $v, true);
        //exibe
        $this->show($html);
    }

    public function receber ($mes = 0, $ano = 0) {

        //

    }

}