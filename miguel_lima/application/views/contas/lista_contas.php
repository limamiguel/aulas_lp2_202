<div class="container">

    <div class="mt-4">
        <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
            Nova Conta
        </a>
    </div>

    <div class="collapse" id="collapseExample">
        <div class="mt-3">
            <div class="row">
                <div class="col-md-6 mx-auto border mt-5 pt-5">
                    <form method="POST" id="contas-form">

                        <input class="form-control" name="parceiro" type="text" placeholder="Devedor/Credor"><br>
                        <input class="form-control" name="descricao" type="text" placeholder="Descrição"><br>
                        <input class="form-control" name="valor" type="number" placeholder="Valor"><br><br>
                        
                        <div class="row">
                            <div class="col-md-6">
                                <input type="number" name="mes" placeholder="Mês" class="form-control">
                            </div>
                            <div class="col-md-6">
                                <input type="number" name="ano" placeholder="Ano" class="form-control">
                            </div>
                        </div>
                        
                        <input type="hidden" name="tipo" value="<?= $tipo ?>">

                        <div class="text-center text-md-left mt-5">
                            <a class="btn btn-primary" onclick="document.getElementById('contas-form').submit();">Enviar</a>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row mt-5">
        <div class="col">
           <?= $lista ?>
        </div>
    </div>

</div>