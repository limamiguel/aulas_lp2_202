<div class="container">
    <div class="col-md-6 mx-auto">
        <form method="POST" class="text-center border border-light p-5" id="contas-form">

            <p class="h4 mb-4"></p>

            <input class="form-control" name="parceiro" type="text" placeholder="Devedor/Credor"><br>
            <input class="form-control" name="descricao" type="text" placeholder="Descrição"><br>
            <input class="form-control" name="valor" type="number" placeholder="Valor"><br><br>
                        
            <div class="row">
                <div class="col-md-6">
                    <input type="number" name="mes" placeholder="Mês" class="form-control">
                </div>
                <div class="col-md-6">
                    <input type="number" name="ano" placeholder="Ano" class="form-control">
                </div>
            </div>
                        
            <div class="row">
                <div class="col-md-6">
                    <div class="custom-control custom-radio">
                        <input type="radio" class="custom-control-input" id="tipo_pagar" name="tipo" value="pagar">
                        <label class="custom-control-label" for="tipo_pagar">Pagar</label>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="custom-control custom-radio">
                        <input type="radio" class="custom-control-input" id="tipo_receber" name="tipo" value="receber">
                        <label class="custom-control-label" for="tipo_receber">Receber</label>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="md-6 offset-md-6">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="liquidada" name="liquidada">
                        <label class="custom-control-label" for="liquidada">Conta Paga</label>
                    </div>
                </div>
            </div>

            <!-- adicionar liquidada ao banco -->

            <!--input type="hidden" name="tipo" id="tipo">-->

            <div class="text-center text-md-left mt-5">
                <button class="btn btn-primary" type="submit">Enviar</button>
            </div>

        </form>
    </div>
</div>