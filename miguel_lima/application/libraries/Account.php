<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH.'libraries/util/CI_Object.php';

class Account extends CI_Object {

    public function salva($data) {
        $data['liquidada'] = isset($data['liquidada']) ? 1 : 0;
        $this->db->insert('conta', $data);
    }

}